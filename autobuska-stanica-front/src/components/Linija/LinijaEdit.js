import React from "react";
import { withNavigation, withParams } from "../../routeconf";
import TestAxios from "../../apis/TestAxios";
import { Button, Form, FormControl, FormGroup, FormSelect, FormLabel } from "react-bootstrap";

class LinijaEdit extends React.Component {
  constructor(props) {
    super(props);

    let linija = {
      brojMesta: 0,
      cenaKarte: 0,
      vremePolaska: "",
      destinacija: "",
      prevoznikId: -1
    }

    this.state = {
      linija: linija,
      prevoznici: []
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    await this.getLinija();
    await this.getPrevoznici();
  }

  async getLinija() {

    try {
      let res = await TestAxios.get("/linije/" + this.props.params.id);
      if (res && res.status === 200) {

        this.setState({
          linija: res.data
        })
      }
    } catch (err) {
      console.log(err)
      alert("Nije uspelo dobavljanje linije.");
    }
  }

  async getPrevoznici() {
    try {
      let res = await TestAxios.get("/prevoznici");
      if (res && res.status === 200) {
        this.setState({
          prevoznici: res.data
        })
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje prevoznika.");
    }
  }

  async edit() {
    try {
      await TestAxios.put("/linije/" + this.props.params.id, this.state.linija);
      this.props.navigate("/linije");
    } catch (error) {
      alert("Nije uspelo čuvanje promena na liniji.");
    }
  }

  valueInputChange(event) {
    let name = event.target.name;
    let value = event.target.value;

    let linija = this.state.linija;

    linija[name] = value;

    this.setState({ linija: linija });
  }

  render() {
    return (<>
      <h2>Izmena linije</h2>
      <Form>
        <FormGroup>
          <FormLabel>
            Broj mesta
          </FormLabel>
          <FormControl
            value={this.state.linija.brojMesta}
            name="brojMesta"
            as="input"
            onChange={(e) => this.valueInputChange(e)}
            type="number"
            min="0"
            step="1"
          ></FormControl>
        </FormGroup>
        <FormGroup>
          <FormLabel>
            Cena karte
          </FormLabel>
          <FormControl
            value={this.state.linija.cenaKarte}
            name="cenaKarte"
            as="input"
            onChange={(e) => this.valueInputChange(e)}
            type="number"
            min="0"
            step="0.1"
          ></FormControl>
        </FormGroup>
        <FormGroup>
          <FormLabel>
            Destinacija
          </FormLabel>
          <FormControl
            value={this.state.linija.destinacija}
            name="destinacija"
            as="input"
            onChange={(e) => this.valueInputChange(e)}
          ></FormControl>
        </FormGroup>
        <FormGroup>
          <FormLabel>
            Vreme polaska
          </FormLabel>
          <FormControl
            value={this.state.linija.vremePolaska}
            name="vremePolaska"
            as="input"
            onChange={(e) => this.valueInputChange(e)}
            type="time"
          ></FormControl>
        </FormGroup>
        <FormGroup>
          <FormLabel>
            Prevoznik
          </FormLabel>
          <FormSelect
            value={this.state.linija.prevoznikId}
            onChange={(e) => this.valueInputChange(e)}
            name="prevoznikId"
          >
            <option value={-1}>Izaberite prevoznika</option>
            {this.state.prevoznici.map((prevoznik) => {
              return (
                <option value={prevoznik.id} key={prevoznik.id}>
                  {prevoznik.naziv}
                </option>
              )
            })}
          </FormSelect>
        </FormGroup>
        <Button style={{ marginTop: "30px" }} onClick={() => this.edit()}>Izmeni</Button>
      </Form></>
    );
  }
}

export default withNavigation(withParams(LinijaEdit));
