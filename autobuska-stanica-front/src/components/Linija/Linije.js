import React from "react";
import { Button, ButtonGroup, Col, Form, FormControl, FormGroup, FormLabel, FormSelect, Table } from "react-bootstrap";
import TestAxios from "../../apis/TestAxios";
import { withNavigation, withParams } from "../../routeconf";

class Linije extends React.Component {

    constructor(props) {
        super(props);

        let linija = {
            brojMesta: 0,
            cenaKarte: 0,
            vremePolaska: "",
            destinacija: "",
            prevoznikId: -1
        }

        this.state = {
            linija: linija,
            linije: [],
            prevoznici: [],
            search: { maxCena: 0, destinacija: "", prevoznikId: -1 },
            pageNo: 0,
            totalPages: 1
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await this.getLinije(0);
        await this.getPrevoznici();
    }

    async getLinije(page) {

        let config = {
            params: {
                pageNo: page
            }
        }

        if (this.state.search.cenaMax != "") {
            config.params.cenaMax = this.state.search.cenaMax;
        }

        if (this.state.search.destinacija != "") {
            config.params.destinacija = this.state.search.destinacija;
        }

        if (this.state.search.prevoznikId != -1) {
            config.params.prevoznikId = this.state.search.prevoznikId;
        }

        try {
            let res = await TestAxios.get("/linije", config);
            if (res && res.status === 200) {

                this.setState({
                    pageNo: page,
                    totalPages: res.headers["total-pages"],
                    linije: res.data
                })
            }
        } catch (err) {
            console.log(err)
            alert("Nije uspelo dobavljanje linija.");
        }
    }

    async getPrevoznici() {
        try {
            let res = await TestAxios.get("/prevoznici");
            if (res && res.status === 200) {
                this.setState({
                    prevoznici: res.data
                })
            }
        } catch (error) {
            alert("Nije uspelo dobavljanje prevoznika.");
        }
    }

    searchValueInputChange(event) {
        let name = event.target.name;
        let value = event.target.value;

        let search = this.state.search;
        search[name] = value;

        this.setState({ search: search });
    }

    async delete(linijaId) {
        try {
            let res = await TestAxios.delete("/linije/" + linijaId);

            if (res && res.status === 204){
                let nextPage;
                if (this.state.pageNo == this.state.totalPages - 1 && this.state.linije.length == 1 && this.state.pageNo != 0) {
                    nextPage = this.state.pageNo - 1
                } else {
                    nextPage = this.state.pageNo
                }

                await this.getLinije(nextPage);
            }
        } catch (err) {
            console.log(err);
            alert("Nije uspelo brisanje linije.");
        }
    }

    formatVreme(vremeFull){

        let prelaz = new String(vremeFull);
        let vreme = prelaz.substring(0,5);

        return vreme;
    }


    goToEdit(id) {
        this.props.navigate("/linije/edit/" + id)
    }

    async create(){
        try{
            let res = await TestAxios.post("/linije", this.state.linija);
            if (res && res.status === 201){
                let form = document.getElementsByName('add-linija')[0];
                form.reset();
            }
        } catch (err) {
            alert("Dodavanje linije nije uspelo!");
        }
    }

    valueInputChange(event) {
        let name = event.target.name;
        let value = event.target.value;

        let linija = this.state.linija;

        linija[name] = value;
        this.setState({ linija : linija });
    }

    async rezervisi(id, mesta){
        let rand = Math.random() * 10;
        rand = Math.round(rand);

        if (rand > mesta){
            alert("Pokusali ste da rezervisete " + rand + " mesta, ali to nije moguce!")
            return;
        }

        let rezervacija = {
            rezervisanaMesta: rand,
            linijaId: id
        }
        try{
            let res = await TestAxios.post("/linije/" + id +"/rezervacije", rezervacija);
            if (res && res.status === 201){
                alert("Uspesno rezervisano " + rand + " mesta!")
                window.location.reload(); 
            }
        } catch (err) {
            alert("Rezervacija nije uspela!");
        }
    }

    renderAdd(){
        return(<>
            <br />
            <br />
            <br />
            <h2>Dodavanje linija</h2>
            <Form name="add-linija">
                <FormGroup>
                    <FormLabel>
                        Broj mesta
                    </FormLabel>
                    <FormControl
                        placeholder="Unesite broj mesta"
                        name="brojMesta"
                        as="input"
                        onChange={(e) => this.valueInputChange(e)}
                        type="number"
                        min="0"
                        step="1"
                    ></FormControl>
                </FormGroup>
                <FormGroup>
                    <FormLabel>
                        Cena karte
                    </FormLabel>
                    <FormControl
                        placeholder="Unesite cenu"
                        name="cenaKarte"
                        as="input"
                        onChange={(e) => this.valueInputChange(e)}
                        type="number"
                        min="0"
                        step="0.1"
                    ></FormControl>
                </FormGroup>
                <FormGroup>
                    <FormLabel>
                        Destinacija
                    </FormLabel>
                    <FormControl
                        placeholder="Unesite odrediste"
                        name="destinacija"
                        as="input"
                        onChange={(e) => this.valueInputChange(e)}
                    ></FormControl>
                </FormGroup>
                <FormGroup>
                    <FormLabel>
                        Vreme polaska
                    </FormLabel>
                    <FormControl
                        placeholder="Izaberite vreme polaska"
                        name="vremePolaska"
                        as="input"
                        onChange={(e) => this.valueInputChange(e)}
                        type="time"
                    ></FormControl>
                </FormGroup>
                <FormGroup>
                    <FormLabel>
                        Prevoznik
                    </FormLabel>
                    <FormSelect
                        onChange={(e) => this.valueInputChange(e)}
                        name="prevoznikId"
                    >
                        <option value={-1}>Izaberite prevoznika</option>
                        {this.state.prevoznici.map((prevoznik) => {
                            return (
                                <option value={prevoznik.id} key={prevoznik.id}>
                                    {prevoznik.naziv}
                                </option>
                            )
                        })}
                    </FormSelect>
                </FormGroup>
                <Button style={{ marginTop: "30px" }} onClick={() => this.create()}>Dodajte</Button>
            </Form>
            <br />
            <br />
            <br />
            <br /></>
        )
    }

    render() {
        return <>

            <h1>Autobuska stanica Subotica</h1>

            {window.localStorage['role'] == "ROLE_ADMIN" ? this.renderAdd() : null}
            
            <h3>Pretraga</h3>
            <Form>
                <FormGroup as={Col}>
                    <FormLabel>
                        Destinacija
                    </FormLabel>
                    <FormControl
                        placeholder="Unesite destinaciju"
                        value={this.state.search.destinacija}
                        name="destinacija"
                        onChange={(e) => this.searchValueInputChange(e)}
                    ></FormControl>
                </FormGroup>
                <FormGroup as={Col}>
                    <FormLabel>
                        Maksimalna cena
                    </FormLabel>
                    <FormControl
                        placeholder="Unesite maksimalnu zeljenu cenu"
                        value={this.state.search.cenaMax}
                        name="cenaMax"
                        as="input"
                        onChange={(e) => this.searchValueInputChange(e)}
                        type="number"
                        min="0"
                        step="0.1"
                    ></FormControl>
                </FormGroup>
                <FormGroup>
                    <FormLabel>
                        Prevoznik
                    </FormLabel>
                    <FormSelect
                        onChange={(e) => this.searchValueInputChange(e)}
                        name="prevoznikId"
                        value={this.state.search.prevoznikId}
                    >
                        <option value={-1}>Izaberite prevoznika</option>
                        {this.state.prevoznici.map((prevoznik) => {
                            return (
                                <option value={prevoznik.id} key={prevoznik.id}>
                                    {prevoznik.naziv}
                                </option>
                            )
                        })}
                    </FormSelect>
                </FormGroup>
                <br />

                <Button onClick={() => this.getLinije(0)}>Trazi</Button>
            </Form>

            <br />
            <br />
            
            <ButtonGroup style={{ marginTop: 5, float: "right" }}>
                <Button
                    style={{ margin: 3, width: 90 }}
                    disabled={this.state.pageNo == 0} onClick={() => this.getLinije(this.state.pageNo - 1)}>
                    Prethodna
                </Button>
                <Button
                    style={{ margin: 3, width: 90 }}
                    disabled={this.state.pageNo >= this.state.totalPages - 1} onClick={() => this.getLinije(this.state.pageNo + 1)}>
                    Sledeca
                </Button>
            </ButtonGroup>

            <Table bordered striped hover style={{ marginTop: 5 }}>
                <thead>
                    <tr>
                        <th>Naziv prevoznika</th>
                        <th>Destinacija</th>
                        <th>Broj mesta</th>
                        <th>Vreme polaska</th>
                        <th>Cena karte</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.linije.map((linija) => {
                        return (
                            <tr key={linija.id}>
                                <td>{linija.prevoznikNaziv}</td>
                                <td>{linija.destinacija}</td>
                                <td>{linija.brojMesta}</td>
                                <td>{this.formatVreme(linija.vremePolaska)}</td>
                                <td>{linija.cenaKarte}</td>

                                {window.localStorage['role'] == "ROLE_ADMIN" ?
                                    <td>
                                        <Button
                                            name="izmeni"
                                            style={{ marginRight: "10px" }}
                                            onClick={(e) => this.goToEdit(linija.id)}>
                                            Izmeni
                                        </Button>

                                        <Button onClick={() => this.delete(linija.id, linija.brojMesta)} variant="danger">
                                            Obrisi
                                        </Button>
                                    </td>
                                    :
                                    <td>
                                        <Button
                                            name="rezervisi"
                                            onClick={(e) => this.rezervisi(linija.id)}
                                            variant="warning">
                                            Rezervisi
                                        </Button>
                                    </td>
                                }
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
            <br />
            <br />
            <br />
            <br />
            <br />
        </>
    }
}

export default withNavigation(withParams(Linije));