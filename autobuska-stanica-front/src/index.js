import React from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router, Routes, Route, Link, Navigate } from "react-router-dom";
import { Navbar, Container, NavbarBrand, Nav, NavLink, Button } from "react-bootstrap";
import { logout } from "./services/auth";
import Login from "./components/authorization/Login";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import Linije from "./components/Linija/Linije";
import LinijaEdit from "./components/Linija/LinijaEdit";


// npm install
// npm install react-router-dom
// npm install axios
// npm install react-bootstrap
// npm install jwt

// npm start

class App extends React.Component {
    render(){
        const jwt = window.localStorage['jwt'];

        if(jwt) {
            return (<>
                <Router>
                    <Navbar expand bg="dark" variant="dark">
                        <NavbarBrand as={Link} to="/">
                            Autobuska Stanica
                        </NavbarBrand>
                        <Nav className="mr-auto">

                            <NavLink as={Link} to="/linije">
                                Linije
                            </NavLink>
                            <Button onClick={()=>logout()}>Logout</Button>
                        </Nav>
                    </Navbar>
                    <Container style={{paddingTop:"10px"}}>
                        <Routes>
                            <Route path="/" element={<Home/>}/>
                            <Route path="/login" element={<Navigate replace to="/"/>}/>
                            <Route path="/linije" element={<Linije/>}/>
                            <Route path="/linije/edit/:id" element={<LinijaEdit/>}/>
                            <Route path="*" element={<NotFound/>}/>
                        </Routes>
                    </Container>
                </Router>
            </>)
        } else {
            return(
                <>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <NavbarBrand as={Link} to="/">
                                Autobuska Stanica
                            </NavbarBrand>
                            <Nav className="mr-auto">
                                <NavLink as={Link} to="/login">
                                    Login
                                </NavLink>
                            </Nav>
                        </Navbar>
                        <Container style={{paddingTop:"10px"}}>
                            <Routes>
                                <Route path="/" element={<Home/>}/>
                                <Route path="/login" element={<Login/>}/>
                                <Route path="*" element={<Navigate replace to="/login"/>}/>
                            </Routes>
                        </Container>
                    </Router>
                </>)
        }
    }
}

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
);