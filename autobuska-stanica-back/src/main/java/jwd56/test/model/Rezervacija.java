package jwd56.test.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Rezervacija {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer rezervisanaMesta;
	
	@ManyToOne
	private Linija linija;

	public Rezervacija() {
		super();
	}

	public Rezervacija(Long id, Integer rezervisanaMesta, Linija linija) {
		super();
		this.id = id;
		this.rezervisanaMesta = rezervisanaMesta;
		this.linija = linija;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rezervacija other = (Rezervacija) obj;
		return Objects.equals(id, other.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getRezervisanaMesta() {
		return rezervisanaMesta;
	}

	public void setRezervisanaMesta(Integer rezervisanaMesta) {
		this.rezervisanaMesta = rezervisanaMesta;
	}

	public Linija getLinija() {
		return linija;
	}

	public void setLinija(Linija linija) {
		this.linija = linija;
	}
	
	
}
