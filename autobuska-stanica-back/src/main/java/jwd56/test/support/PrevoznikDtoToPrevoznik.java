package jwd56.test.support;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Linija;
import jwd56.test.model.Prevoznik;
import jwd56.test.service.LinijaService;
import jwd56.test.service.PrevoznikService;
import jwd56.test.web.dto.PrevoznikDTO;

@Component
public class PrevoznikDtoToPrevoznik implements Converter<PrevoznikDTO, Prevoznik> {

	@Autowired
	private PrevoznikService prevoznikService;
	
	@Autowired
	private LinijaService linijaService;
	
	@Override
	public Prevoznik convert(PrevoznikDTO dto) {
		
		Prevoznik entitet;
		
		if (dto.getId() == null) {
			entitet = new Prevoznik();
		} else {
			entitet = prevoznikService.findOne(dto.getId());
		}
		
		if (entitet != null) {
			entitet.setNaziv(dto.getNaziv());
			entitet.setAdresa(dto.getAdresa());
			entitet.setPib(dto.getPib());
			
			List<Linija> linije = dto.getLinije().stream().map(linijaService::findOne).collect(Collectors.toList());
			entitet.setLinije(linije);
		}
		return entitet;
	}
}