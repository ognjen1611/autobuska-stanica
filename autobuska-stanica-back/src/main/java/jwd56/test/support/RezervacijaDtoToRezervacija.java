package jwd56.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Linija;
import jwd56.test.model.Rezervacija;
import jwd56.test.service.LinijaService;
import jwd56.test.service.RezervacijaService;
import jwd56.test.web.dto.RezervacijaDTO;

@Component
public class RezervacijaDtoToRezervacija implements Converter<RezervacijaDTO, Rezervacija> {

	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private LinijaService linijaService;
	
	@Override
	public Rezervacija convert(RezervacijaDTO dto) {
		
		Rezervacija entitet;
		
		if (dto.getId() == null) {
			entitet = new Rezervacija();
		} else {
			entitet = rezervacijaService.findOne(dto.getId());
		}
		
		if (entitet != null) {
			entitet.setRezervisanaMesta(dto.getRezervisanaMesta());
			Linija linija = linijaService.findOne(dto.getLinijaId());
			entitet.setLinija(linija);
		}
		return entitet;
	}
}