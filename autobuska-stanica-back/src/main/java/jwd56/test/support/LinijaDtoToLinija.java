package jwd56.test.support;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Linija;
import jwd56.test.model.Prevoznik;
import jwd56.test.service.LinijaService;
import jwd56.test.service.PrevoznikService;
import jwd56.test.web.dto.LinijaDTO;

@Component
public class LinijaDtoToLinija implements Converter<LinijaDTO, Linija> {

	@Autowired
	private LinijaService linijaService;
	
	@Autowired
	private PrevoznikService prevoznikService;
	
	@Override
	public Linija convert(LinijaDTO dto) {
		
		Linija entitet;
		
		if (dto.getId() == null) {
			entitet = new Linija();
		} else {
			entitet = linijaService.findOne(dto.getId());
		}
		
		if (entitet != null) {
			entitet.setBrojMesta(dto.getBrojMesta());
			entitet.setCenaKarte(dto.getCenaKarte());
			entitet.setVremePolaska(dto.getVremePolaska());
			entitet.setDestinacija(dto.getDestinacija());
			
			Prevoznik prevoznik = prevoznikService.findOne(dto.getPrevoznikId());
			entitet.setPrevoznik(prevoznik);
		}
		return entitet;
	}
	
    @SuppressWarnings("unused")
	private LocalTime getLocalTime(String time) throws DateTimeParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return LocalTime.parse(time, formatter);
    }

}