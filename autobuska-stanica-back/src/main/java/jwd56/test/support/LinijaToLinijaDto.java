package jwd56.test.support;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Linija;
import jwd56.test.model.Rezervacija;
import jwd56.test.web.dto.LinijaDTO;

@Component
public class LinijaToLinijaDto implements Converter<Linija, LinijaDTO> {

	@Override
	public LinijaDTO convert(Linija source) {
		LinijaDTO dto = new LinijaDTO();
		
		dto.setId(source.getId());
		
		Integer rezervacije = source.getRezervacije().stream().map(Rezervacija::getRezervisanaMesta).collect(Collectors.summingInt(Integer::intValue));
		Integer trenutnoStanje = source.getBrojMesta() - rezervacije;
		
		dto.setBrojMesta(trenutnoStanje);
		dto.setCenaKarte(source.getCenaKarte());
		dto.setVremePolaska(source.getVremePolaska());
		dto.setDestinacija(source.getDestinacija());
		dto.setPrevoznikId(source.getPrevoznik().getId());
		dto.setPrevoznikNaziv(source.getPrevoznik().getNaziv());
		return dto;
	}
	
	public List<LinijaDTO> convert(List<Linija> lista){
		return lista.stream().map(this::convert).collect(Collectors.toList());
	}
}
