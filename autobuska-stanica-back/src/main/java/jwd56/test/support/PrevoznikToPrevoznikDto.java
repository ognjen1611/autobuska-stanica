package jwd56.test.support;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Linija;
import jwd56.test.model.Prevoznik;
import jwd56.test.web.dto.PrevoznikDTO;

@Component
public class PrevoznikToPrevoznikDto implements Converter<Prevoznik, PrevoznikDTO> {

	@Override
	public PrevoznikDTO convert(Prevoznik source) {
		PrevoznikDTO dto = new PrevoznikDTO();
		
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setAdresa(source.getAdresa());
		dto.setPib(source.getPib());
		
		List<Long> linije = source.getLinije().stream().map(Linija::getId).collect(Collectors.toList());
		dto.setLinije(linije);
		
		return dto;
	}
	
	public List<PrevoznikDTO> convert(List<Prevoznik> lista){
		return lista.stream().map(this::convert).collect(Collectors.toList());
	}
}
