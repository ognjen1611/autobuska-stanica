package jwd56.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import jwd56.test.model.Linija;

public interface LinijaService {

	Linija findOne(Long id);
	List<Linija> findAll();
	
	List<Linija> findByPrevoznikId (Long prevoznikId);

	Linija save(Linija entitet);

	Linija update(Linija entitet);

	Linija delete(Long id);

	Page<Linija> search(Double cenaMax, String destinacija, Long prevoznikId, int pageNo);

}
