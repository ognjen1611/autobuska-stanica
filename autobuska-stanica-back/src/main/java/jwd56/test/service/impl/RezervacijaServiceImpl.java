package jwd56.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd56.test.model.Rezervacija;
import jwd56.test.repository.RezervacijaRepository;
import jwd56.test.service.RezervacijaService;

@Service
public class RezervacijaServiceImpl implements RezervacijaService {
	
	@Autowired
	private RezervacijaRepository rezervacijaRepository;

	@Override
	public Rezervacija findOne(Long id) {
		return rezervacijaRepository.findOneById(id);
	}

	@Override
	public List<Rezervacija> findAll() {
		return rezervacijaRepository.findAll();
	}

	@Override
	public Rezervacija save(Rezervacija rezervacija) {
		return rezervacijaRepository.save(rezervacija);
	}

	@Override
	public List<Rezervacija> findByLinijaId(Long linijaId) {
		return rezervacijaRepository.findByLinijaId(linijaId);
	}
}
