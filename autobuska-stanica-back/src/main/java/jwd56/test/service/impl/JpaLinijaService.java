package jwd56.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd56.test.model.Linija;
import jwd56.test.repository.LinijaRepository;
import jwd56.test.service.LinijaService;

@Service
public class JpaLinijaService implements LinijaService {

	@Autowired
	private LinijaRepository linijaRepository;
	
	@Override
	public Linija findOne(Long id) {
		return linijaRepository.findOneById(id);
	}

	@Override
	public List<Linija> findAll() {
		return linijaRepository.findAll();
	}

	@Override
	public Linija save(Linija entitet) {
		return linijaRepository.save(entitet);
	}

	@Override
	public Linija update(Linija entitet) {
		return linijaRepository.save(entitet);
	}

	@Override
	public Linija delete(Long id) {
		Optional<Linija> opt = linijaRepository.findById(id);
		if (opt.isPresent()) {
			linijaRepository.deleteById(id);
			return opt.get();
		}
		return null;
	}

	@Override
	public Page<Linija> search(Double cenaMax, String destinacija, Long prevoznikId, int pageNo) {
		
		if (cenaMax == null) {
			cenaMax = Double.MAX_VALUE;
		}
		
		if (destinacija == null) {
			destinacija = "";
		}
		
		if (prevoznikId == null) {
			return linijaRepository.findByCenaKarteLessThanEqualAndDestinacijaIgnoreCaseContains(cenaMax, destinacija, PageRequest.of(pageNo, 3));
		} else {
			return linijaRepository.findByCenaKarteLessThanEqualAndDestinacijaIgnoreCaseContainsAndPrevoznikId(cenaMax, destinacija, prevoznikId, PageRequest.of(pageNo, 3));
		}
	}

	@Override
	public List<Linija> findByPrevoznikId(Long prevoznikId) {
		return linijaRepository.findByPrevoznikId(prevoznikId);
	}

}
