package jwd56.test.service;

import java.util.List;

import jwd56.test.model.Prevoznik;

public interface PrevoznikService {

	Prevoznik findOne(Long id);
	List<Prevoznik> findAll();
	
	Prevoznik save(Prevoznik entitet);

}
