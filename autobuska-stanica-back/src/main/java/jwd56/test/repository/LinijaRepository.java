package jwd56.test.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd56.test.model.Linija;

@Repository
public interface LinijaRepository extends JpaRepository<Linija, Long> {
	
	Linija findOneById(Long id);
	
	List<Linija> findByPrevoznikId(Long prevoznikId);
	
	Page<Linija> findByCenaKarteLessThanEqualAndDestinacijaIgnoreCaseContains(Double cenaMax, String destinacija, Pageable pageable);
	
	Page<Linija> findByCenaKarteLessThanEqualAndDestinacijaIgnoreCaseContainsAndPrevoznikId(Double cenaMax, String destinacija, Long prevoznikId, Pageable pageable);
}