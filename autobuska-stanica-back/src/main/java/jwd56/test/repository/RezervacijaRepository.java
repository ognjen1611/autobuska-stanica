package jwd56.test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd56.test.model.Rezervacija;

@Repository
public interface RezervacijaRepository extends JpaRepository<Rezervacija, Long> {
	
	Rezervacija findOneById(Long id);
	
	List<Rezervacija> findByLinijaId(Long linijaId);

}