package jwd56.test.web.dto;

import javax.validation.constraints.NotNull;

public class RezervacijaDTO {
	
	private Long id;
	
	private Integer rezervisanaMesta;
	
	@NotNull
	private Long linijaId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getRezervisanaMesta() {
		return rezervisanaMesta;
	}

	public void setRezervisanaMesta(Integer rezervisanaMesta) {
		this.rezervisanaMesta = rezervisanaMesta;
	}

	public Long getLinijaId() {
		return linijaId;
	}

	public void setLinijaId(Long linijaId) {
		this.linijaId = linijaId;
	}
	
	
}
