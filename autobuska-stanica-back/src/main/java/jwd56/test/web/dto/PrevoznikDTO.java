package jwd56.test.web.dto;

import java.util.List;

public class PrevoznikDTO {
	
	private Long id;
	
	private String naziv;
	
	private String adresa;
	
	private String pib;
	
	private List<Long> linije;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getPib() {
		return pib;
	}

	public void setPib(String pib) {
		this.pib = pib;
	}

	public List<Long> getLinije() {
		return linije;
	}

	public void setLinije(List<Long> linije) {
		this.linije = linije;
	}
	
	
}
