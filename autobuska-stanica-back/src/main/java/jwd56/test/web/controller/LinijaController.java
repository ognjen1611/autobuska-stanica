package jwd56.test.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd56.test.model.Linija;
import jwd56.test.model.Rezervacija;
import jwd56.test.service.LinijaService;
import jwd56.test.service.RezervacijaService;
import jwd56.test.support.LinijaDtoToLinija;
import jwd56.test.support.LinijaToLinijaDto;
import jwd56.test.support.RezervacijaDtoToRezervacija;
import jwd56.test.web.dto.LinijaDTO;
import jwd56.test.web.dto.RezervacijaDTO;

@RestController
@RequestMapping(value = "/api/linije", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class LinijaController {
	
	@Autowired
	private LinijaDtoToLinija fromDto;

	@Autowired
	private LinijaToLinijaDto toDto;
	
	@Autowired
	private RezervacijaDtoToRezervacija rezFromDto;
	
	@Autowired
	private RezervacijaService rezService;
	
	@Autowired
	private LinijaService service;
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDTO> create(@Valid @RequestBody LinijaDTO dto){
		Linija entitet = fromDto.convert(dto);
		Linija vrati = service.save(entitet);
		
		return new ResponseEntity<LinijaDTO>(toDto.convert(vrati), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('ROLE_KORISNIK')")
	@PostMapping(value = "/{id}/rezervacije", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> create(@RequestBody @Valid RezervacijaDTO dto){
		Rezervacija entitet = rezFromDto.convert(dto);
		rezService.save(entitet);
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<LinijaDTO>> getAll(
			@RequestParam(required = false) Double cenaMax,
			@RequestParam(required = false) String destinacija,
			@RequestParam(required = false) Long prevoznikId,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo
			){
		
		Page<Linija> rezultati = service.search(cenaMax, destinacija, prevoznikId, pageNo);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(rezultati.getTotalPages()));
		return new ResponseEntity<List<LinijaDTO>>(toDto.convert(rezultati.getContent()), headers, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<LinijaDTO> getOne(@PathVariable Long id){
		Linija entitet = service.findOne(id);
		
		if (entitet != null) {
			return new ResponseEntity<LinijaDTO>(toDto.convert(entitet), HttpStatus.OK);
		} else {
			return new ResponseEntity<LinijaDTO>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDTO> update(@PathVariable Long id, @Valid @RequestBody LinijaDTO dto){
		
		if (!id.equals(dto.getId())) {
			return new ResponseEntity<LinijaDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Linija entitet = fromDto.convert(dto);
		Linija vrati = service.update(entitet);
		
		return new ResponseEntity<LinijaDTO>(toDto.convert(vrati), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		
		Linija obrisan = service.delete(id);
		
		if (obrisan != null) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
	
    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
