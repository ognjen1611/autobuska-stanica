package jwd56.test.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jwd56.test.model.Prevoznik;
import jwd56.test.service.PrevoznikService;
import jwd56.test.support.PrevoznikDtoToPrevoznik;
import jwd56.test.support.PrevoznikToPrevoznikDto;
import jwd56.test.web.dto.PrevoznikDTO;

@RestController
@RequestMapping(value = "/api/prevoznici", produces = MediaType.APPLICATION_JSON_VALUE)
public class PrevoznikController {
	
	@Autowired
	private PrevoznikDtoToPrevoznik fromDto;

	@Autowired
	private PrevoznikToPrevoznikDto toDto;
	
	@Autowired
	private PrevoznikService service;
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrevoznikDTO> create(@Valid @RequestBody PrevoznikDTO dto){
		Prevoznik entitet = fromDto.convert(dto);
		Prevoznik vrati = service.save(entitet);
		
		return new ResponseEntity<PrevoznikDTO>(toDto.convert(vrati), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<PrevoznikDTO>> getAll(){
		
		List<Prevoznik> prevoznici = service.findAll();
		return new ResponseEntity<List<PrevoznikDTO>>(toDto.convert(prevoznici), HttpStatus.OK);
	}

    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
