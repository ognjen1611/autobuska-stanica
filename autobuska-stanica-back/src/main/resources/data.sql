INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
              
INSERT INTO `autobuska-stanica`.`prevoznik` (`id`, `adresa`, `naziv`, `pib`) VALUES ('1', 'Kulina Bana 4', 'Savaturs', '123456789');
INSERT INTO `autobuska-stanica`.`prevoznik` (`id`, `adresa`, `naziv`, `pib`) VALUES ('2', 'Novaka Nesudjenog 13', 'Tenis trans', '222222222');
INSERT INTO `autobuska-stanica`.`prevoznik` (`id`, `adresa`, `naziv`, `pib`) VALUES ('3', 'Ajs Nigrutina 43 23', 'Chavo Kotrljajuci', '987654321');

INSERT INTO `autobuska-stanica`.`linija` (`id`, `broj_mesta`, `cena_karte`, `destinacija`, `vreme_polaska`, `prevoznik_id`) VALUES ('1', '56', '750', 'Beograd', '21:00:00', '1');
INSERT INTO `autobuska-stanica`.`linija` (`id`, `broj_mesta`, `cena_karte`, `destinacija`, `vreme_polaska`, `prevoznik_id`) VALUES ('2', '40', '850', 'Nis', '22:00:00', '1');
INSERT INTO `autobuska-stanica`.`linija` (`id`, `broj_mesta`, `cena_karte`, `destinacija`, `vreme_polaska`, `prevoznik_id`) VALUES ('3', '60', '700', 'Mali Mokri Lug', '01:00:00', '2');
INSERT INTO `autobuska-stanica`.`linija` (`id`, `broj_mesta`, `cena_karte`, `destinacija`, `vreme_polaska`, `prevoznik_id`) VALUES ('4', '50', '650', 'Indjija', '11:30:00', '2');
INSERT INTO `autobuska-stanica`.`linija` (`id`, `broj_mesta`, `cena_karte`, `destinacija`, `vreme_polaska`, `prevoznik_id`) VALUES ('5', '52', '950', 'Backi Jarak', '12:15:00', '3');
INSERT INTO `autobuska-stanica`.`linija` (`id`, `broj_mesta`, `cena_karte`, `destinacija`, `vreme_polaska`, `prevoznik_id`) VALUES ('6', '48', '450', 'Sombor', '13:35:00', '3');
INSERT INTO `autobuska-stanica`.`linija` (`id`, `broj_mesta`, `cena_karte`, `destinacija`, `vreme_polaska`, `prevoznik_id`) VALUES ('7', '32', '1050', 'Segedin', '20:00:00', '3');